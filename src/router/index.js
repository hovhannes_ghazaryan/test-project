import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Main from '@/containers/Main'
// Views
import OperationManuList from '@/views/operationMenu/List'

Vue.use(Router)

let router = new Router({
    mode: 'history',
    fallback: false,
    routes: [
        {
            path: '/',
            redirect: '/operation-menu',
            name: 'Main',
            component: Main,
            children: [
                {
                    path: '/operation-menu',
                    name: 'OperationManu',
                    component: OperationManuList
                }
            ]
        }
    ]
})

export default router
