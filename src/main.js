import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueI18n from 'vue-i18n'
import ru from './lang/locals/ru.json'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
  faAngleLeft,
  faAngleRight,
  faAngleUp,
  faAngleDown,
  faArrowUp,
  faArrowDown,
  faSearch,
  faFilter,
  faPlus,
  faList,
  faArrowsAltH,
  faChartBar,
  faHammer,
  faAlignJustify,
  faSlidersH,
  faStar
} from '@fortawesome/free-solid-svg-icons'

import {
  faCalendar,
  faCopy,
  faUser,
  faQuestionCircle
} from '@fortawesome/free-regular-svg-icons'

import {
  faReact
} from '@fortawesome/free-brands-svg-icons'

library.add(
    faAngleLeft,
    faAngleRight,
    faAngleUp,
    faAngleDown,
    faArrowUp,
    faArrowDown,
    faSearch,
    faFilter,
    faPlus,
    faList,
    faArrowsAltH,
    faChartBar,
    faHammer,
    faAlignJustify,
    faSlidersH,
    faStar,
    faCalendar,
    faCopy,
    faUser,
    faQuestionCircle,
    faReact
)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueI18n)

Vue.config.productionTip = false

const messages = {
  ru: ru
}

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'ru', // set locale
  messages // set locale messages
})

const eventHub = new Vue()
Vue.prototype.$eventHub = eventHub

new Vue({
  el: '#app',
  router,
  vuetify,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
