import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {
  dataDays: [
    {
      title: 'Сегодня, 10 июля',
      dataByDay: [
        {
          progressStatus: 'up',
          paymentStatus: 'paid',
          amountProgress: 'up',
          price: {
            price: '+50 239',
            remainder: ',17 ₽'
          },
          data: [
            { title: 'ИП Емельянов Антон Викторович', description_title: 'Со счёта:', description: 'Тинькофф банк' },
            { title: 'Сайт Volkswagen', description_title: 'Продвижение:' },
            { title: 'Оплата за продвижение Яндекс.Директ июнь-июль' }
          ]
        },
        {
          progressStatus: 'down',
          paymentStatus: 'paid',
          amountProgress: 'down',
          price: {
            price: '-200 000',
            remainder: ',00 ₽'
          },
          data: [
            { title: 'Дмитрий Воропаев', description_title: 'Со счёта:', description: 'Тинькофф банк' },
            { title: 'ИМ Bonafide', description_title: 'Зарплата сотрудникам:' },
            { title: 'Аванс Июль' }
          ]
        },
        {
          progressStatus: 'up',
          paymentStatus: 'paid',
          amountProgress: 'tremendous',
          price: {
            price: '+500 000',
            remainder: ',00 ₽'
          },
          data: [
            { title: 'ООО «Большие турбины»', description_title: 'Со счёта:', description: 'Тинькофф банк' },
            { title: 'Сайт Большие турбины', description_title: 'Разработка' },
            { title: 'Аванс за разработку' }
          ]
        }

      ]
    },
    {
      title: 'Вчера, 9 июля',
      dataByDay: [
        {
          progressStatus: 'up',
          paymentStatus: 'paid',
          amountProgress: 'down',
          price: {
            price: '-20 000',
            remainder: ',00 ₽'
          },
          data: [
            { title: 'Дмитрий Воропаев', description_title: 'Со счёта:', description: 'Тинькофф банк' },
            { title: 'Обучение сотрудников', description_title: 'Расходы на офис' },
            { title: 'Оплата обучения' }
          ]
        },
        {
          progressStatus: 'up',
          paymentStatus: 'paid',
          amountProgress: 'tremendous',
          price: {
            price: '+500 000',
            remainder: ',00 ₽'
          },
          data: [
            { title: 'ООО «Большие турбины»', description_title: 'Со счёта:', description: 'Тинькофф банк' },
            { title: 'Сайт Большие турбины', description_title: 'Разработка' },
            { title: 'Аванс за разработку' }
          ]
        },
        {
          progressStatus: 'up',
          paymentStatus: 'paid',
          amountProgress: 'up',
          price: {
            price: '+50 239',
            remainder: ',17 ₽'
          },
          data: [
            { title: 'ИП Емельянов Антон Викторович', description_title: 'Со счёта:', description: 'Тинькофф банк' },
            { title: 'Сайт Volkswagen', description_title: 'Продвижение' },
            { title: 'Оплата за продвижение Яндекс.Директ июнь-июль' }
          ]
        }

      ]
    }
  ]
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
