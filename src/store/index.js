import Vue from 'vue'
import Vuex from 'vuex'
import operationMenu from './modules/operationMenu'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  namespaced: true,
  modules: {
    operationMenu
  }
})
